import React from 'react';
import './App.css';
import Login from './components/login'
import Signup from "./components/signup"
import Home from "./components/home"
import { Link } from 'react-router-dom';
import{Route,Routes} from 'react-router-dom';



function App (){
  return (
     <div className="App">
<Routes>
      <Route  path="/" element={<Home/>} /> 
      <Route   path="/login" element={<Login/>} /> 
      <Route path="/signup"element={<Signup/>} /> 
</Routes>
    </div>
  )
}
export default App;


// import React, { Component } from "react";
// import { BrowserRouter, Route, Switch } from "react-router-dom";
// import Home from "./components/Home";
// import Signup from "./components/signup/Signup";
// import Login from "./components/login/Login";
// import Dashboard from "./components/dashboard/Dashboard";

// class App extends Component {
//   render() {
//     return (
//       <div>
//         <BrowserRouter>
//           <Switch>
//             <Route path="/signup" component={Signup} />
//             <Route path="/login" c