import React,{useState} from 'react'
import {useNavigate} from 'react-router-dom';
import axios from "axios"
  
const Signup = () => {
    const navigate=useNavigate()
    const [data,setData]=useState({
       name:'',
       email:'',
       password:'',
       confirmPassword:'', 
    })
    const {name,email,password,confirmPassword}=data;
    const changeHandler=e=>{
        setData({...data,[e.target.name]:e.target.value})

    }
    const submitHandler=e=>{
       e.preventDefault();
       if (password===confirmPassword){
           console.log(data);
        }else{
            console.log("passwords are not same ") 
        }
    }
    const signup=()=>{
        console.log("errors")
        const {name,email,password,confirmPassword}=data;
        if( (name && email && password) && (password === confirmPassword)){
            console.log(data,"this is the data")
            axios.post("http://localhost:5000/api/users", data)
            .then((res)=>{
                alert("registered successfully");
                navigate("/login")
            }).catch((err)=>{
                alert(err)
            })
        } 

    }
    return(
        <div className="signup">
            {console.log(data)}
            <center>
            <div className="signup page">
                <h2>signup</h2>
            </div>
                <form onSubmit={submitHandler}>
                    <input type='text' name ="name" value={data.name}  onChange={changeHandler}placeholder= "Enter UserName"/> <br/>
                    <input type='email'name="email" value={data.email} onChange={changeHandler} placeholder= "Email or phone number"/><br/>
                    <input type='password'name="password" value={data.password} onChange={changeHandler} placeholder= "password"/><br/>
                    <input type='password' name="confirmPassword" value={data.confirmPassword} onChange={changeHandler} placeholder= "Confirm password"/> <br/>
                    {/* <input type="submit" name ="submit"/> <br/>                 */}
                    <button className="button" onClick={signup}>submit</button>
                </form>
            </center>
        </div>
    )
}
export default Signup;