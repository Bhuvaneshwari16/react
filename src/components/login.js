// import React from 'react'
// import { useState } from 'react'
// import{Link}from 'react-router-dom';
// import axios from 'axios'
// const Login = () => {
//   const [userName,setUserName]=useState("")
//   const [password,setPassword]=useState("")
//   const passwordHandler=(e) => {
//     setPassword(e.target.value);
//     console.log(password)
//   };
//   const nameHandler=(e) =>{
//     setUserName(e.target.value)
//   }
//   const login=(e)=>{
//     const loginData={"email":userName,"password":password}
//     axios.post("http://localhost:5000/api/auth",loginData)
//     .then((res)=>
//       console.log(res)
//     )
//   }
//     return (
//     <div login>
//     <input type="text" placeholder= "username" value ={userName}  onChange={nameHandler}></input>
//     <input type="password" placeholder="password" value={password} onChange ={passwordHandler}></input>
//     <button type="button" onClick={login}>submit</button>
//     <p>
//     <a href = "a">Forget password ?</a>or<Link to="/signup">Signup</Link>
//     {/* <a href="#" onClick={()=>handleChange("event",1)}>signup</a></Link> */}
//     </p>
//     </div>
//   )
// }
// export default Login;


import React ,{useState}from 'react'
import{useNavigate}from 'react-router-dom';
import axios from 'axios';
// import Signup  from './signup';
const Login = () => {
  const navigate=useNavigate();
  const [userName,setUserName]=useState("")
  const [password,setPassword]=useState("")
  const passwordHandler=(e) => {
    setPassword(e.target.value);
    console.log(password)
  };
  const nameHandler=(e) =>{
    setUserName(e.target.value)
  }
  const login=(e)=>{
    const loginData={"email":userName,"password":password}
    axios.post("http://localhost:5000/api/auth",loginData)
    .then((res)=>{
      // local storage
      window.localStorage.setItem('user', JSON.stringify(res.data));
      // window.localStorage.getItem('user');
      // JSON.parse(window.localStorage.getItem('user'));
      if(res.data.password===password)
      alert("login successfully")
      navigate("/")
    }
      // console.log(res)
    )
  }
  return (
    <div>
      <div className="seconddiv">
      <center>
        <img src="https://img.favpng.com/0/9/22/computer-icons-user-profile-clip-art-login-png-favpng-uFvp1vcYYXhXh1gMT3Cai7ikf.jpg"  alt="profile" className="profile"/>        
        <div className="h1">
        <h2>login page</h2>
        </div>
        <form>
          <input type="text" name ="username" value={userName} onChange={nameHandler} placeholder="username"/><br/>
          <input type="password" name="password" value={password} onChange={passwordHandler} placeholder="password"/><br/>
          <button type="button" onClick={login}>submit</button>    
          </form>
          <p>
          <a href = "a">Forget password ?</a>or<button onClick ={()=>navigate("/signup")}>signup</button>
            {/* <a href="#" onClick={()=>handleChange("event",1)}>signup</a></Link> */}
          </p>
      </center>
      </div>
    </div>
  )
}

export default Login;
